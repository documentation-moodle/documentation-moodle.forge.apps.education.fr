---
sidebar_position: 6
---

# Le suivi d'achèvement

**Dans Moodle, l'enseignant dispose de 2 paramètres pour individualiser les parcours d'apprentissage.**

:::info
 * **Les Restrictions d'accès** : ce paramètre permet de déterminer les conditions qui permettront d'accéder à une activité ou à une ressource
 * **Le Suivi d'achèvement** : ce paramètre permet de déterminer à quelle condition une activité sera considérée comme terminée. Cela permettra d'accéder à la suite du parcours, de recevoir un badge…  
**Ces 2 paramètres peuvent être combinés. Nous allons déjà découvrir le suivi d'achèvement.**
:::

## Le suivi d'achèvement

**Le suivi d'achèvement permet d'indiquer qu'une activité est achevée ou non.** Une case apparaît à côté de chaque activité, qui se coche lorsque l'étudiant a terminé l'activité selon les critères d'achèvement. 

:::danger
**Par défaut, ce paramètre n'est pas activé dans votre cours Moodle.**  
Pour l'activer : ![](img/achevement01.jpg#printscreen#droite#colonne)

**1.**	Accédez aux **paramètres** de votre cours : 

![](img/achevement02.jpg#printscreen#droite#colonne)

**2.**	Dans les paramètres, à la rubrique **"suivi d'achèvement"**, il faut **"activer le suivi d'achèvement des activités"**.

**NB :**  Un nouveau paramètre permet de choisir si mes conditoons seront affichées sur la page de cours (en plus de l'affichage dans le page de l'activité). 
:::

➡️ Ouvrez les paramètres de la ressource ou de l'activité

### Conditions d'achèvement pour une Ressource (ex. ressource Fichier ou URL)

Il est possible de choisir l'une de ces 2 conditions :
 * ![](img/achevement14.jpg#icone) Choix 1 : "Les participants peuvent marquer manuellement cette activité comme terminée" : ce sont les élèves qui devront cocher la case à droite de la ressource pour certifier qu'ils l'ont consultée.

![](img/achevement03.jpg#printscreen#droite#colonne)

 * ![](img/achevement15.jpg#icone) Choix 2 : "Afficher l'activité comme terminée dès que les conditions sont remplies" : Dans le cas d'une ressource, la seule condition possible est d'avoir affiché cette activité pour la terminer. 
*(ex. les élèves doivent ouvrir un fichier PDF pour que cela soit considéré comme terminé)*
*Affichage sur la page de l'élève :* 

![](img/achevement04.jpg#printscreen)


### Conditions d'achèvement pour une Activité (ex. un TEST)

**Les conditions d'achèvement dépendent de chaque activité.**

![](img/achevement05.jpg#printscreen#droite#colonne)

Par exemple, pour un test, il est possible de paramétrer ces conditions :

 * ![](img/achevement14.jpg#icone) Choix 1 : "Les participants peuvent marquer manuellement cette activité comme terminée".

 * Choix 2 : "Afficher l'activité comme terminée dès que les conditions sont remplies". 
	
    Dans ce cas, Il y a plusieurs possibilités : 
    a. *Les étudiants doivent afficher l'activité pour la terminer*  
    b. *Les étudiants doivent recevoir une note pour terminer cette activité :*  
    ➡️ *Requiert la note de passage* : l'activité est considérée comme terminée lorsque l'étudiant reçoit une note suffisante (à définir dans la rubrique "Note" des paramètres de l'activité).  
    ➡️ *Ou toutes les tentatives terminées* : dans le cas où les élèves ont droit à un nombre précis de tentatives pour faire le test (à définir dans le paramètre Note > Nombre de tentatives autorisées) 
:::info  **Exemple 1 : Le TEST sera terminé avec une note minimale**

 ➡️ Ouvrez les **Paramètres** de l'activité  

 ![](img/achevement06.jpg#printscreen#droite#colonne)

 ➡️ Dans la rubrique **"Notes"** :

 **1.**	Indiquez la **"note pour passer".**
 **2.**	Indiquez le **"nombre de tentatives autorisées".**
 
 

 ![](img/achevement07.jpg#printscreen#droite#colonne)

 ➡️ Dans la rubrique **"Achèvement d'activité"** :

 **3.**	Sélectionnez **"Afficher l'activité comme terminée dès que les conditions sont remplies".**
 
 **4.**	Cochez les cases **"L'étudiant doit recevoir une note…"**, **"L'étudiant doit obtenir une note minimale de réussite"**
 et **"ou toutes les tentatives terminées".**
 
 **5.**	**Enregistrez**

   ![](img/achevement15.jpg#icone) (Coche bleue) N'importe quelle note permet d'achever l'activité.  

   ![](img/achevement16.jpg#icone) (Coche rouge) L'activité a été faite, mais sans atteindre la note minimale.  

   ![](img/achevement17.jpg#icone) (Coche verte) L'activité a été faite avec une note supérieure ou égale à la note minimale.
         
:::
:::info  **Exemple 2 : L'activité DEVOIR sera terminée quand l'élève aura déposé un travail** (sans condition de note).

 ➡️ Ouvrez les **Paramètres** de l'activité  

 ![](img/achevement08.jpg#printscreen#droite#colonne)

 ➡️ Dans la rubrique **"Achèvement d'activité"** :

**1.** Sélectionnez **"Afficher l'activité comme terminée dès que les conditions sont remplies".**

**2.** Cochez la case "L**e participant doit remettre quelque chose pour terminer cette activité".**

**3.** **Enregistrez**
  
:::

## Le suivi d'achèvement côté Élève

 ➡️ Dans le cours, tous les granules paramétrés avec un suivi d'achèvement sont associés une information.

**Visible dès la page de cours** (voir l'encadré au début de cette fiche) :

 ![](img/achevement09.jpg#printscreen#droite#colonne)

 

**Visible dans la page de l'activité** :

 ![](img/achevement10.jpg#printscreen#droite#colonne)


## Le suivi d'achèvement côté Enseignant

![](img/achevement11.jpg#printscreen#droite#colonne)

**1.**	Dans votre cours, cliquez sur l'onglet **"Rapports".**

**2.**	Cliquez sur le menu **"Achèvement d'activités".**

![](img/achevement12.jpg#printscreen#droite#colonne)

**3.**	Les **rapports d'achèvement d'activités** présentent…
 * La **liste des élèves** inscrits
 * L'achèvement de **chacune des activités paramétrées**

**Un export au format Tableur est possible.**

:::tip
![](img/achevement13.jpg#printscreen#droite#colonne)

**Le bloc "Progression"** permet de rendre plus visuelle la progression dans le cours :

*Consultez la fiche Réflexe qui vous présente ce bloc.*
:::
